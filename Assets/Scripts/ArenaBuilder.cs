using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class ArenaBuilder : MonoBehaviour
{
    void Start()
    {
        var rand = new System.Random();
        Vector3 startTransform = new Vector3(-0.5f * length, -0.5f * height, 0);
        for (int i = 0; i < length; i++)
        {
            for (int j = 0; j < height; j++)
            {
                spawnPosition = startTransform + new Vector3(i, j, 0);
                if (rand.NextDouble() > obstacleDensity)
                {
                    Instantiate(emptyField, spawnPosition, Quaternion.identity);
                }
                else
                {
                    Instantiate(obstacle, spawnPosition, Quaternion.identity);
                }
            }
        }
        
        for (int i = -1; i < length+1; i++)
        {
            spawnPosition = startTransform + new Vector3(i, -1, 0);
            Instantiate(obstacle, spawnPosition, Quaternion.identity);
            spawnPosition = startTransform + new Vector3(i, height, 0);
            Instantiate(obstacle, spawnPosition, Quaternion.identity);
        }
        for (int i = 0; i < height; i++)
        {
            spawnPosition = startTransform + new Vector3(-1, i, 0);
            Instantiate(obstacle, spawnPosition, Quaternion.identity);
            spawnPosition = startTransform + new Vector3(length, i, 0);
            Instantiate(obstacle, spawnPosition, Quaternion.identity);
        }
    }


    public int length;
    public int height;
    public float obstacleDensity;
    [SerializeField] private GameObject emptyField;
    [SerializeField] private GameObject obstacle;

    private Vector3 spawnPosition;
}
