using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionAvoidance : MonoBehaviour
{
    void Start()
    {
        robotEngine = gameObject.GetComponent<Engine>();
        InvokeRepeating("CheckForObstacles", 0.1f, 0.1f);
    }
    private void Update()
    {
        // Front Sensor
        frontSensor.x = Mathf.Cos(robotEngine.direction);
        frontSensor.y = Mathf.Sin(robotEngine.direction);
        frontSensor = frontSensor.normalized;
        // Left sensor
        leftSensor.x = -Mathf.Cos(robotEngine.direction - sensorAngle);
        leftSensor.y = -Mathf.Sin(robotEngine.direction - sensorAngle);
        leftSensor = leftSensor.normalized;
        // Right sensor
        rightSensor.x = -Mathf.Cos(robotEngine.direction + sensorAngle);
        rightSensor.y = -Mathf.Sin(robotEngine.direction + sensorAngle);
        rightSensor = rightSensor.normalized;
        // Debug Rays
        Debug.DrawRay(transform.position, frontSensor * sensorDistance, Color.green);
        Debug.DrawRay(transform.position, leftSensor * sensorDistance, Color.green);
        Debug.DrawRay(transform.position, rightSensor * sensorDistance, Color.green);
    }

    void CheckForObstacles()
    {
        RaycastHit2D hitFront = Physics2D.Raycast(transform.position, leftSensor * sensorDistance);
        if (hitFront.collider != null && hitFront.distance < 1f)
        {
            Debug.Log("Obstacle ahead: " + hitFront.collider.name);
            robotEngine.acceleration -= -2;
        }
        RaycastHit2D hitLeft = Physics2D.Raycast(transform.position, leftSensor * sensorDistance);
        if (hitLeft.collider != null && hitLeft.distance < 3f)
        {
            Debug.Log("Obstacle on the left: " + hitLeft.collider.name);
            robotEngine.steeringAngle -= 90;
        }

        RaycastHit2D hitRight = Physics2D.Raycast(transform.position, rightSensor * sensorDistance);
        if (hitRight.collider != null && hitRight.distance < 3f)
        {
            Debug.Log("Obstacle on the right: " + hitRight.collider.name);
            robotEngine.steeringAngle += 90;
        }


    }
    public int sensorAngle;
    public int sensorDistance;
    private Vector2 leftSensor;
    private Vector2 rightSensor;
    private Vector2 frontSensor;
    private Engine robotEngine;
}
