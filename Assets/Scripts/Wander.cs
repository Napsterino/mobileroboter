using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wander : MonoBehaviour
{
    void Start()
    {
        robotEngine = gameObject.GetComponent<Engine>();
        rand = new System.Random(System.Guid.NewGuid().GetHashCode());
        InvokeRepeating("ChangeDirection", 0.5f, 0.5f);
    }

    void ChangeDirection()
    {
        robotEngine.acceleration += wanderAcceleration;
        steeringDirection = rand.Next(40) - 20;
        robotEngine.steeringAngle += steeringDirection;
    }
    private Engine robotEngine;
    private int steeringDirection;
    private float wanderAcceleration = 1f;
    private System.Random rand;
}
