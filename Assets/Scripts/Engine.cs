using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Engine : MonoBehaviour
{
    void Start()
    {     
        rigidBody = gameObject.GetComponent<Rigidbody2D>();
        direction = Mathf.Deg2Rad * gameObject.transform.localEulerAngles.z;
    }
    void Update()
    {     
        // Acceleration
        acceleration = Mathf.Clamp(acceleration, maxReverseSpeed, maxSpeed);

        // Steering
        steeringAngle = Mathf.Clamp(steeringAngle, -45, 45);
        transform.Rotate(new Vector3(0, 0, steeringAngle) * Time.deltaTime * acceleration);
        direction = Mathf.Deg2Rad * gameObject.transform.localEulerAngles.z;

        // Applying force
        rigidBody.velocity = transform.right * acceleration;
    }
    public float acceleration;
    public float steeringAngle = 0f;
    public float direction;
    public float maxSpeed;
    public float maxReverseSpeed;

    private Vector2 forceVector;
    private Rigidbody2D rigidBody;
}
